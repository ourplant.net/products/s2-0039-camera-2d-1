| Parameter                                                    | Value                            | Ermittlung |
| :----------------------------------------------------------- | -------------------------------- | ---------- |
| **General:**                                                 |                                  |            |
| Dimensions in mm (W x D x H)                                 | 49x265x400                       |            |
| Weight in kg                                                 | 5                                |            |
| Compressed air range in bar (+specifications if applicable)  |                                  |            |
| Voltage in V/DC (V/AC)                                       | 48                               |            |
| Max. current in A                                            | 5                                |            |
| Communication interface                                      | Ethernet                         |            |
| Functional temperature range in °C                           | +20...+35                        |            |
| Max. relative humidity in %                                  | < 60                             |            |
| Electronic connectors (Stecker - Ende des Kabels was vom Produkt abgeht) | 48V Anschluss Ethernet Anschluss |            |
| Pneumatic connectors (Stecker - Ende des Kabels was vom Produkt abgeht) |                                  |            |
| **Movements:**                                               |                                  |            |
| Movement range in Z in mm                                    | 150                              |            |
| Accuracy z-axis in mm                                        |                                  |            |
| Max. Speed Z-axis in mm/s                                    |                                  |            |
| Axis resolution Z-axis in µm                                 |                                  |            |
| Z movement (component height) in mm                          |                                  |            |
| Movement range rotation axis /A-axis in degrees              |                                  |            |
| Resolution rotation axis in arcsec                           |                                  |            |
| Movement range of swivel axis (flip/rotation angle) in degrees |                                  |            |
| Movement range Hexapod in mm (X-, Y-, Z-axis)                |                                  |            |
| Movement range Hexapod (Θx-, Θy-, Θz-axis) in degrees        |                                  |            |
| Movement range of the needle system in mm (X, Y)             |                                  |            |
| Stroke of the needle system in mm                            |                                  |            |
| Stroke in mm                                                 |                                  |            |
| Measuring path in mm                                         |                                  |            |
| Opening path in mm                                           |                                  |            |
| **Camera:**                                                  |                                  |            |
| Lens type / Type of objective                                | Telecentric lens                 |            |
| Field of view in mm (W x H)                                  | 0,75 x 0,5                       |            |
| Camera resolution in mm/(µm)                                 | 1                                |            |
| **Touch probe:**                                             |                                  |            |
| Probe accuracy in µm                                         |                                  |            |
| Probe repeatability in µm                                    |                                  |            |
| Max. permissible shear force in N                            |                                  |            |
| **Direct Dispensing:**                                       |                                  |            |
| Adjustment accuracy in mm                                    |                                  |            |
| Adjustment range in mm                                       |                                  |            |
| **Dispensing:**                                              |                                  |            |
| Cartridge holder/size                                        |                                  |            |
| Min. dispensing volume in µl                                 |                                  |            |
| Min. diameter metering point in mm                           |                                  |            |
| Max. Temperature needle heating in °C                        |                                  |            |
| Capacity of drip tray in ml                                  |                                  |            |
| **Laser:**                                                   |                                  |            |
| Emission spectrum in nm                                      |                                  |            |
| Wavelength in nm                                             |                                  |            |
| Min. diameter laser spot in mm                               |                                  |            |
| Working distance in mm                                       |                                  |            |
| Laser type                                                   |                                  |            |
| Pyrometer properties                                         |                                  |            |
| **Substrate sizes / capacities:**                            |                                  |            |
| Max. substrate size in inch / mm (B x T)                     |                                  |            |
| Min. substrate height / thickness in mm                      |                                  |            |
| Max. Magazingröße in Zoll / mm (B x T)                       |                                  |            |
| Anzahl der Magazine                                          |                                  |            |
| Max. wafer size in inch / mm                                 |                                  |            |
| Capacity Waffel Pack Adapter (EWW)                           |                                  |            |
| Capacty magazine (EWW)                                       |                                  |            |
| Substratbreite in mm                                         |                                  |            |
| **Other:**                                                   |                                  |            |
| Max. Stacking height in mm                                   |                                  |            |
| Force measuring range in N                                   |                                  |            |
| Force in N                                                   |                                  |            |
| Max. moving load in kg                                       |                                  |            |
| Permissible product weight on the Hexapod in kg              |                                  |            |
| Max. Weight for Z-stroke in kg                               |                                  |            |
| Max. Load capacity in kg                                     |                                  |            |
| Max. Temperature tool in °C                                  |                                  |            |
| Max. Temperature in °C                                       |                                  |            |
| Power in W                                                   |                                  |            |
| Lighting                                                     |                                  |            |
| Time per flip                                                |                                  |            |
| Theoretical force at 6 bar, forward flow                     |                                  |            |
| **Machine:**                                                 |                                  |            |
| Set-up area / cencter-to-center distance of the machine feet in mm (W x D) |                                  |            |
| Außenabmessungen in mm (W x D x H)                           |                                  |            |
| Max. functional area in mm (X,Y)                             |                                  |            |
| Repeatability in mm                                          |                                  |            |
| Max. load X-axis in kg                                       |                                  |            |
| Max. load Y-axis in kg                                       |                                  |            |
| Max. speed X-axis in mm/s                                    |                                  |            |
| Max. speed Y-axis in mm/s                                    |                                  |            |
| Max. acceleration X-axis in mm/s²                            |                                  |            |
| Max. acceleration Y-axis in mm/s²                            |                                  |            |
| Power supply                                                 |                                  |            |
| Operating Voltage                                            |                                  |            |
| Energy requirements in kW                                    |                                  |            |
| Certification                                                |                                  |            |
| Min. load capacity table in kg                               |                                  |            |
| Transport dimensions in mm (W x D x H)                       |                                  |            |
| Dimensions of the machine feet in mm (diameter/B x T)        |                                  |            |
| Minimum distance to surrounding objects (front and back) in mm |                                  |            |