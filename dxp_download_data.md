Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1).



| document | download options |
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/01_operating_manual/S2-0039_A_BA_Camera.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/01_operating_manual/S2-0039_A_OM_Camera.pdf)|
|assembly drawing           |[de/en](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/02_assembly_drawing/s2-0039-A_ZNB_camera.pdf)|
|circuit diagram         | [de/en](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/03_circuit_diagram/S2-0039_A_EPLAN_Camera.pdf)|
|maintenance instructions          |[de](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/04_maintenance_instructions/S2-0039_A_WA_Camera.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/04_maintenance_instructions/S2-0039_A_MI_Camera.pdf)                  |
| spare parts          | [de](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/05_spare_parts/S2-0039_A_EVL_Camera.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0039-camera-2d-1/-/raw/main/05_spare_parts/S2-0039_A_SWP_Camera.pdf)                 |

